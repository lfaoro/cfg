#!/usr/bin/env zsh

## Yubikey SSH
#export GPG_TTY="$(tty)"
#export SSH_AUTH_SOCK=$(gpgconf --list-dirs agent-ssh-socket)
#gpg-connect-agent updatestartuptty /bye > /dev/null

export HISTCONTROL=ignorespace
export TERM="xterm-256color"
export GIT_TERMINAL_PROMPT=1
export ZSH="$HOME/.oh-my-zsh"

# Secrets is where to store access keys as environment
# variables.
secrets="$HOME/.secrets"
if [[ ! -x $secrets ]]; then
	touch "$secrets"
fi
source "$secrets"

# User configuration
export PATH="/usr/local/bin:/usr/local/sbin:/usr/bin:/bin:/usr/sbin:/sbin"

# Go | https://golang.org
mkdir -p "$HOME/go"
export GOPATH="$HOME/go"
export PATH="$GOPATH/bin:$PATH"
export PATH="$PATH:/usr/local/go/bin"
export GOPRIVATE="*"
export GONOPROXY="*"
export GONOSUMDB="*"

# You may need to manually set your language environment
export LANG=en_US.UTF-8
export LANGUAGE=en_US.UTF-8
export LC_ALL=en_US.UTF-8
export LC_CTYPE=en_US.UTF-8

export HOMEBREW_NO_ANALYTICS=1

# fix cloudsdk python issue
export CLOUDSDK_PYTHON="$(which python3)"

## Oh my ZSH config
ZSH_THEME="ys"
# Uncomment the following line to enable command auto-correction.
ENABLE_CORRECTION="false"
# Uncomment the following line to display red dots whilst waiting for completion.
COMPLETION_WAITING_DOTS="true"
# Uncomment the following line if you want to change the command execution time
# stamp shown in the history command output.
# The optional three formats: "mm/dd/yyyy"|"dd.mm.yyyy"|"yyyy-mm-dd"
# HIST_STAMPS="myyyy-mm-dd"
# Which plugins would you like to load?
# Standard plugins can be found in ~/.oh-my-zsh/plugins/*
# Custom plugins may be added to ~/.oh-my-zsh/custom/plugins/
# Example format: plugins=(rails git textmate ruby lighthouse)
# Add wisely, as too many plugins slow down shell startup.
plugins=(
    git
    osx
    fzf
    autojump
    encode64
    golang
    helm
    nmap
    rsync
    urltools
)

source "$HOME/.oh-my-zsh/oh-my-zsh.sh" || :
source "$HOME/.alias/init.zsh"

## enable kubernetes prompt
export K8S_PROMPT=false
if [[ ${K8S_PROMPT} = "true" ]]; then
    source "/usr/local/opt/kube-ps1/share/kube-ps1.sh"
    PS1='$(kube_ps1)'$PS1
fi

bindkey "\e\eOD" backward-word 
bindkey "\e\eOC" forward-word

[ -f ~/.fzf.zsh ] && source ~/.fzf.zsh

# heroku autocomplete setup
HEROKU_AC_ZSH_SETUP_PATH=/Users/lfaoro/Library/Caches/heroku/autocomplete/zsh_setup && test -f $HEROKU_AC_ZSH_SETUP_PATH && source $HEROKU_AC_ZSH_SETUP_PATH;