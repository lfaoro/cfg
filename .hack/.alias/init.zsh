#!/usr/bin/env zsh

# Set personal aliases, overriding those provided by oh-my-zsh libs,
# plugins, and themes. Aliases can be placed here, though oh-my-zsh
# users are encouraged to define aliases within the ZSH_CUSTOM folder.
# For a full list of active aliases, run `alias`.
# Example aliases
# alias zshconfig="mate ~/.zshrc"
# alias ohmyzsh="mate ~/.oh-my-zsh"
# [ -f ~/.fzf.zsh ] && source ~/.fzf.zsh

## OS specific settings
if [[ "$OSTYPE" == "linux-gnu" ]]; then
	echo "Incredible, you're using Linux!"
	source "$HOME/.alias/linux.zsh"
elif [[ "$OSTYPE" =~ ^darwin. ]]; then
	echo "Awesome! You're running macOS"
	source "$HOME/.alias/macos.zsh"
	source "$HOME/.alias/misc.zsh"
else
	echo "Unknown OS"
fi

## emacs
alias e='emacs -nw '
alias ew='emacs '

# Disk utility aliases
alias df='df -h'
alias du='du -h'

# File search functions
function f() { command find . -iname "*$1*" ${@:2} }
function r() { command grep "$1" ${@:2} -R . }

function install_ohmyzsh() {
	rm -rf $HOME/.oh-my-zsh
	sh -c "$(curl -fsSL https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"
	mv $HOME/.zshrc.pre-oh-my-zsh $HOME/.zshrc
}

function cfg() {
	/usr/bin/git --git-dir=$HOME/.cfg/ --work-tree=$HOME "$@"
}
function cfgupd() {
    cfg commit -am 'update'
    cfg push --force
}
function cfgupdres() {
    first=$(cfg rev-list --max-parents=0 --abbrev-commit HEAD)
    cfg reset --soft ${first}
    cfg commit -am 'update'
    cfg push --force
}
alias cfs='cfg status'

function addalias() {
	echo "alias $1" >> "$HOME/.alias/macos.zsh"
	source "$HOME/.zshrc"
}

function gcd() {
	cd "$GOPATH/src/$1"
}

## kubernetes
# kubectl () {
#     command kubectl $*
#     if [[ -z $KUBECTL_COMPLETE ]]
#     then
#         source <(command kubectl completion zsh)
#         KUBECTL_COMPLETE=1
#         echo "exported"
#     fi
# }
# alias k='kubectl '

function kubectl() { echo "+ kubectl $@"; command kubectl $@; }
[ -f ~/.alias/kubectl_aliases ] && source ~/.alias/kubectl_aliases

# Syntax explanation
# k=kubectl
# sys=--namespace kube-system
# commands:
# g=get
# d=describe
# rm=delete
# a:apply -f
# ex: exec -i -t
# lo: logs -f
# resources:
# po=pod, dep=deployment, ing=ingress, svc=service, cm=configmap, sec=secret, ns=namespace, no=node
# flags:
# output format: oyaml, ojson, owide
# all: --all or --all-namespaces depending on the command
# sl: --show-labels
# w=-w/--watch
# value flags (should be at the end):
# f=-f/--filename
# l=-l/--selector


## SSL
# CSR - generates a Certificate Signing Request for a wildcard domain
generateCSR() {
	if [[ -f $(which openssl) ]]; then
		openssl req -nodes -newkey rsa:2048 \
			-keyout $1.key \
			-out $1.csr \
			-subj "/C=MT/ST=Pieta/L=Pieta/O=Alpha34 Ltd./OU=ArenaCube/CN=*.$1"
	else

		echo "openssl is not installed"
	fi
}

spoofMAC() {
	openssl rand -hex 6 | sed 's%\(..\)%\1:%g; s%.$%%'
}

# Letsencrypt
alias renewCerts="./letsencrypt-auto -vvv --agree-tos --renew-by-default --expand \
--email lfaoro@me.com \
--domains "$*""
# --redirect

alias encrypt="openssl aes-256-cbc -a -salt -k $CRYPT_SECRET"
alias decrypt="openssl aes-256-cbc -d -a -salt -k $CRYPT_SECRET"

## Misc
alias cls='clear'
alias ll='ls -GhaltrF'
alias h='history 25'
alias week='date +%V'
alias t='tree -C -L 2'
alias newPass='openssl rand -base64 16 | cut -c1-16'
alias mkdir='mkdir -p'
alias d="docker"

alias src='source $HOME/.zshrc'
alias g="grep --color --exclude-dir={.bzr,CVS,.git,.hg,.svn} -C3 -nr $1 $2"
alias grepEmail="grep -R --text -i -o '[A-Z0-9._%+-]\+@[A-Z0-9.-]\+\.[A-Z]\{2,4\}' *"
alias netstat='netstat -ltunp'
alias upd='apt update && apt full-upgrade -y'
alias sshgenkey='ssh-keygen -t rsa -b 4096 -C "$(whoami)@$(hostname -f)"'
alias ns='netstat -tnlp'
alias finodes="for i in "$1"; do echo $i; find $i |wc -l; don"
alias renameall="find . -type f -iname $1 -exec rename s/$2/ {} +"
gen-password() {
	openssl rand -base64 "$1"
}
lc() {
	find . -type f -name "*.$1" -not -path "./$2/*" | xargs wc -l | sort
}

# git settings
alias gs='git status'
alias ga='git add --all'
alias gc="git commit -vs -m $1"
alias gp='git pull --stat --verbose'
alias gpp='git push --verbose'
alias gl='git log --stat --color'
alias glo="git log --oneline --decorate --graph"
alias gbr='git branch --remote'
alias gpu='git push --verbose'
alias gr='git reset --soft HEAD~'
function grm() {
	git rm --cached "$*"
}
alias gsub='git submodule init && git submodule update'
alias grm-ignored='git ls-files --ignored --exclude-standard | xargs git rm --cached'
function gac() {
	git pull --stat --verbose
	git add --all
	git commit -vsam "$*"
	git push -v
}
function gt() {
	git tag -a $1 -m $1 -f || :
	git push --tags
}
alias gb='git branch'

alias gSubUpdate='git submodule foreach git pull'

# Importing transfer.sh script
source $HOME/.hack/transfer.sh

# Backup
backup() {
	rsync --recursive --update -P \
		--exclude='$RECYCLE.BIN' --exclude='$Recycle.Bin' --exclude='.AppleDB' --exclude='.AppleDesktop' --exclude='.AppleDouble' --exclude='.com.apple.timemachine.supported' --exclude='.dbfseventsd' --exclude='.DocumentRevisions-V100*' --exclude='.DS_Store' --exclude='.fseventsd' --exclude='.PKInstallSandboxManager' --exclude='.Spotlight*' --exclude='.SymAV*' --exclude='.symSchedScanLockxz' --exclude='.TemporaryItems' --exclude='.Trash*' --exclude='.vol' --exclude='.VolumeIcon.icns' --exclude='Desktop DB' --exclude='Desktop DF' --exclude='hiberfil.sys' --exclude='lost+found' --exclude='Network Trash Folder' --exclude='pagefile.sys' --exclude='Recycled' --exclude='RECYCLER' --exclude='System Volume Information' --exclude='Temporary Items' --exclude='Thumbs.db' \
		"$1" "$2"
}

## MISC
alias listen='watch sudo lsof -iTCP -sTCP:LISTEN -P -n'
function kuberun(){
    kubectl run -i --tty --rm $1 --image=$1 --restart=Never -- sh
}
