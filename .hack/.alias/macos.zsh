#!/usr/bin/env zsh

# Completion done through oh-my-zsh
# fpath=(/usr/local/share/zsh-completions $fpath)
# chmod go-w '/usr/local/share'
# rm "$HOME/.zcomp*" &>/dev/null
# autoload bashcompinit && compinit

# brew.sh
export PATH="/usr/local/bin:$PATH"
## gnubin
export PATH="/usr/local/opt/inetutils/libexec/gnubin:$PATH"
export MANPATH="/usr/local/opt/inetutils/libexec/gnuman:$MANPATH"

# SSH
export SSH_KEY_PATH="$HOME/.ssh/lfaoro"

# iTerm
source "$HOME/.iterm2_shell_integration.zsh" || :

# gcloud
if [ -x `which gcloud`  ]; then
  source '/usr/local/Caskroom/google-cloud-sdk/latest/google-cloud-sdk/path.zsh.inc'
  source '/usr/local/Caskroom/google-cloud-sdk/latest/google-cloud-sdk/completion.zsh.inc'
fi

if [ -x `which kubectl` ]; then
  source <(kubectl completion zsh)
fi

if [[ -e `which helm` ]]; then
  source <(helm completion zsh)
fi

# if [[ -e `which aws` ]]; then
#   source /usr/local/share/zsh/site-functions/aws_zsh_completer.sh
# fi

# Misc
function bootout() {
  sudo launchctl bootout user/"$(id -u $1)"
}
alias flush='sudo dscacheutil -flushcache && sudo killall -HUP mDNSResponder'
alias pro='cd ~/Projects/arena'
alias drmexit="docker rm -f $(docker ps -aqf status=exited)"
alias cpkey="cat $HOME/.ssh/lfaoro.pub|pbcopy"
alias a="ansible"
alias ap='ansible-playbook -v'
alias ag="ansible-galaxy -vvv"
alias ansible-galaxy="ag"
## Docker
alias d='docker'

fuse() {
  if [[ "$1" = "" ]]; then
    echo "must provide remote host"
  fi
  sudo mkdir -p "$HOME/sshfs"
  sudo sshfs -o allow_other,defer_permissions,IdentityFile="$HOME/.ssh/lfaoro" "$1:/" "$HOME/sshfs"
}

function brewUpdate() {
    echo "Taking care of CLI tools";echo
    bash -c "brew update && brew upgrade && brew cleanup --force && brew cask cleanup" &

    echo "Forcing re-install of all brew apps";echo
    for cask in $(brew cask list); do
        brew cask install --force $cask
    done

    echo "Cleaning up everything";echo
    brew cask cleanup
}

function brewInstall() {
    "$HOME/.cmd/install_apps.sh"
}

# Kubernetes
ksecretFile() {
  # name / fileName / path to file
  kubectl create secret generic $1 --from-file=$2=$3
}

alias kubealloc='kubectl describe nodes | grep -A 2 -e "^\\s*CPU Requests"'
kpod() {
  kubectl exec -it "$1" "$2"
}
klogs() {
  kubectl logs -f "$1"
}
kns() {
  kubectl config set-context $(kubectl config current-context) --namespace=$1
}

kevent() {
  kubectl get events  --sort-by='.metadata.creationTimestamp'  -o 'go-template={{range .items}}{{.involvedObject.name}}{{"\t"}}{{.involvedObject.kind}}{{"\t"}}{{.message}}{{"\t"}}{{.reason}}{{"\t"}}{{.type}}{{"\t"}}{{.firstTimestamp}}{{"\n"}}{{end}}' --watch
}

retry() {
  echo "to be implemented"
}
