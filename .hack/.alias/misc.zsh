alias vim='emacs -nw '
alias wa='watch '
function renext() {
    for f in *.$1; do
        mv -- "$f" "${f%.$1}.$2"
    done
}
alias d='docker '
