#!/usr/bin/env zsh

## System Maintenance
#alias upgrade='nohup apt update && apt full-upgrade -y >>/dev/null &' ## needs rewriting
alias sysSetup="apt install -y \
                apt-utils \
                arp-scan \
                fail2ban \
                htop \
                iptraf \
                mc \
                mtr \
                ncdu \
                traceroute \
                unzip \
                vim \
                apticron \
                ufw \
                zsh \
                git \
                debian-goodies \
                tree \
                strace \
                rsync \
                lsyncd \
                make \
                curl "

alias sysUpdate="apt update \
                && apt -y full-upgrade \
                && apt-get -y autoclean \
                && apt-get -y autoremove && \
                if [ -f /var/run/reboot-required ]; then echo \"*** Reboot Required ***\"; else echo ""; fi"

function openSSL_endDate() {
	for pem in /etc/ssl/certs/*.pem; do
		printf '%s: %s\n' \
				"$(date --date="$(openssl x509 -enddate -noout -in "$pem"|cut -d= -f 2)" --iso-8601)" \
				"$pem"
	done | sort
}
