#!/usr/bin/env bash

brew install cask \
zsh bash htop ctop tree tmux watch \
wget hping iftop nmap nethogs \
git git-lfs jsoncpp jq xz openssl \
go shellcheck node python3  \
llvm sqlite vim exercism \
ansible docker awscli kubectl speedtest_cli \
asciinema autoconf autojump automake coreutils \
curl emacs fzf gnupg httpie iftop inetutils \
kubernetes-cli kubernetes-helm kubetail ncurses readline \
whois xz zsh

brew install lnav --with-curl

brew cask install iterm2 angry-ip-scanner daisydisk docker filezilla lulu alfred authy bloomrpc daisydisk \
ngrok onyx postman transmission screenflow textual vlc \
google-chrome brave-browser firefox tor-browser \
istumbler paw sizeup skype 1password whatsapp \
slack teamviewer viscosity visual-studio-code vlc wireshark \
dropbox font-fira-code google-cloud-sdk keybase tg-pro

# Install v3 of Dash
brew cask install
https://raw.githubusercontent.com/caskroom/homebrew-cask/0b088ffec44f55713b94bee5b169c3a5e87aed1f/Casks/dash.rb

go get -v -u github.com/mvdan/sh/cmd/shfmt
go get -v -u github.com/derekparker/delve/cmd/dlv
