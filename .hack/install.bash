#!/usr/bin/env bash

set -e

if ! command -v zsh; then
  echo "cfg dotfiles require zsh shell to be installed."
fi

if [[ -e "$HOME/.cfg" ]]; then
  echo "Abort. cfg is already installed."
  exit 1
fi

if [[ ! -e "$HOME/.oh-my-zsh" ]]; then
  echo "Would you like to install OhMyZSH?"
  select yn in "y" "n"; do
    case $yn in
    y)
      sh -c "$(curl -fsSL https://raw.github.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"
      break
      ;;
    n) return ;;
    esac
  done
fi

git clone --bare https://gitlab.com/lfaoro/cfg.git "$HOME/.cfg"

function cfg() {
  /usr/bin/git --git-dir="$HOME/.cfg/" --work-tree="$HOME" "$@"
}

if ! cfg checkout; then
  echo "Backing up pre-existing dot files."
  mkdir -p .config-backup
  cfg checkout 2>&1 | grep -E "\s+\." | awk "{print $1}" | xargs -I{} mv {} .config-backup/{}
fi

cfg checkout
cfg config status.showUntrackedFiles no

if [[ "$OSTYPE" =~ ^darwin. ]]; then
  dscl . -read "/Users/$USER" UserShell
  sudo dscl . -create "/Users/$USER" UserShell "$(which zsh)"
fi
