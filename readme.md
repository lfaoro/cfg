# Dotfiles aka Configuration Files

This repository contains the default configuration I use on Unix based operating systems (e.g. macOS/Linux).
These defaults are usually hidden by prefixing a dot(.) in front of the file name; hence dotfiles.

The gist of it is pretty simple:
- we create a directory where the git-tree lives (.cfg)
- we create a command to manipulate the tree from anywhere on the file system `/usr/bin/git --git-dir=$HOME/.cfg/ --work-tree=$HOME "$@"`
- we exclude seeing all the untracked files `cfg config status.showUntrackedFiles no`
- we add to the tree only what we want to track manually

## Installation
```bash
curl -Lks https://bit.do/lcfg | bash
```

## Usage example
```bash
cfg status
cfg add .gitconfig
cfg commit -m "add gitconfig"
cfg add .bashrc
cfg commit -m "add bashrc"
cfg push
```

## Organization
- .bin | script files / utilities
- .secrets | api keys (not added to the tree)
