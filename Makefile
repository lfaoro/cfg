BACKUPDIR=/Volumes/backup/home/${USER}
#BACKUPDIR=/tmp/${USER}/

backup:
	rsync -aP -vv -h \
	--delete-before \
	--ignore-errors \
	--progress \
	--include-from="${HOME}/.hack/rsync.include" \
	--exclude-from="${HOME}/.hack/rsync.ignore" \
	--exclude="*" \
	${HOME}/ ${BACKUPDIR}

cloud:
	gsutil -m \
	rsync -d -r -j -u -n \
	/tmp/${USER} gs://leo-backup/${USER}

archive:
	gtar --exclude-from=${HOME}/.fileignore \
	--exclude-vcs-ignores \
	--exclude-caches \
	--exclude-backup \
	-I 'pixz -0' \
	-cv -f lfaoro.tar.xz ${HOME}


ignorelist:
	# https://github.com/rubo77/rsync-homedir-excludes
	# add your exclusions
	wget https://raw.githubusercontent.com/rubo77/rsync-homedir-excludes/master/rsync-homedir-excludes.txt -O rsync.ignore &>/dev/null

ohmyzsh:
	$(shell sh -c "$(curl -fsSL https://raw.github.com/robbyrussell/oh-my-zsh/master/tools/install.sh)")

brew.sh:
	$(shell /usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)")

setup_macos:
	$(shell bash -c ".hack/setup_macos.bash")
